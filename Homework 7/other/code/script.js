function CreateNewUser(firstName = prompt("Your name"), lastName = prompt("Your surname"),
    birthday = prompt("Your birthday", "dd.mm.yyyy")) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
}
CreateNewUser.prototype.getLogin = function () {
    return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
}
CreateNewUser.prototype.getAge = function () {
    let bday = this.birthday.split(".");
    let temp = bday[0];
    bday[0] = bday[2];
    bday[2] = temp;
    bday = bday.join("-");

    bday = new Date(Date.parse(bday));

    return Math.floor(Math.abs((Date.now() - bday.getTime()) / (365 * 24 * 3600 * 1000)));
}
CreateNewUser.prototype.getPassword = function () {
    return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.split(".")[2]}`;
}

let u1 = new CreateNewUser();
console.log(u1);
console.log(u1.getLogin());
console.log(u1.getAge());
console.log(u1.getPassword());

function filterBy(arr = [], type) {
    let res = [];
    arr.forEach(el => {
        if (typeof el !== type)
            res.push(el);
    });
    return res;
}
let arr = ['hello', 23, 'world', null, '11'];
console.log("by string " + filterBy(arr, 'string').join(" "));
console.log("by number " + filterBy(arr, 'number').join(" "));