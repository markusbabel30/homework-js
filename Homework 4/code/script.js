const map = (fn, array) => {
    return fn(array);
};

const mapped = (arr) => {
    const newArray = arr.map((digit) => digit * 2);
    return newArray;
};

const updatedArr = map(mapped, [1, 2, 3]);
console.log(updatedArr);

function checkAge(age) {
    return age > 18 ? true : confirm("Родители разрешили?");
    // age > 18 || confirm("Родители разрешили?"); - alternative solution
}
console.log(checkAge(23));
console.log(checkAge(23));